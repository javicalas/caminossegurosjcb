var app = {	

	/**
	 * Funciones para la aplicación de Amigos cercanos
	 * Utiliza las capas de caminosseguros y vecinos
	 * 
	 * @class
	 * @public
	 * @alias caminosseguros.amigos
	*/
		
    map:				null,		//Mapa
    vecinosInicial:		null,		//Datos de vecinos iniciales
    puntoLocalizado: 	null, 		//Punto geolocalizado
    puntoCercano: 		null,		//Punto más cercano actual
    marcador:			null,		//Estilo de marcador para el vecino cercano
    marcadorVecinoCercano: null,	//Marcador de vecino cercano en el mapa

 	
	/**
	 * Inicio de la aplicación. 
	 * Inicializa FastClick y los vecinos cercanos
	 * Crea el estilo para el marcador de amigo cercano
	 * @public
	 */
    inicioApp: function(){
 		FastClick.attach(document.body);	//Iniciamos FastClick
 		app.vecinosInicial 	= jQuery.extend(true, {}, vecinosfinjs);
 		
 	    app.marcador =  L.AwesomeMarkers.icon({
 	    	icon: "coffee",
 	    	prefix:"fa",
 	    	markerColor: "red"
 	    });
    },
    
	/**
	 * Manejador del evento de dispositivo listo
	 * Crea el mapa y define los manejadores de botones
	 * @public
	 */
    dispositivoListo: function(){
    	app.crearMapa();
    	
    	//Manejadores de botones
        $("#button-geo").click(app.onBtnGeolocalizar);
        $("#buscarOtro").click(app.onBtnBuscarOtro);
    },
    
    
	/**
	 * Creación del mapa, maneja sus eventos y añade las capas
	 * @public
	 */
    crearMapa: function(){
    	//Creamos mapa
    	app.map = L.map("vecino",{
	    	fadeAnimation: 			true,
	        zoomAnimation: 			false,
	        markerZoomAnimation: 	false
	    }).fitWorld();
    	
    	//Manejamos eventos de geolocalización
    	//app.map.on("locationfound", app.onLocationFound);
    	//app.map.on("locationerror", app.onLocationError);

    	//Añadimos capa base
		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoicGVkcm9jYWxhcyIsImEiOiJjajJ4cjluMTMwMThtMndxOGg4Y3NoZWkzIn0.83VnuZwLtidmCHxuP1briA', {
			maxZoom: 18,
			attribution: '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(app.map);
		
    	//Añadimos capa de caminos seguros y geolocalizamos
		app.anyadirCapaAMapa(caminosfinjs);
		app.geolocalizar();
		
		var biciStyle = {
	        	color: "#30B13B",
	        	weight: 4,
	        	opacity: 0.7,
	        	fillColor: "black",
	        	lineJoin: "round"
	        };

	    var layerCarrilBici = L.geoJson(bicicletafinjs,{style: biciStyle});
	    layerCarrilBici.addTo(app.map);
    	
    },
    
	/**
	 * Crea una capa GeoJson y la añade al mapa
	 * @param {Object} datosCapa Datos de la capa
	 * @public
	 */
    anyadirCapaAMapa: function(datosCapa){
        var layer = L.geoJson(datosCapa,{});
        layer.addTo(app.map);
    },
    
    
	/**
	 * Manejador del evento Geolocalización encontrada
	 * @param {evt} Object Evento
	 * @public
	 */
    
   
    
    onLocationFound: function(evt){
    	 
    	var marcadorGeo = L.AwesomeMarkers.icon({
    	    	icon: 'child',
    	    	prefix:'fa',
    	    	markerColor: 'blue'
    	    	});

    	app.puntoLocalizado = [evt.coords.longitude, evt.coords.latitude];
    	//app.puntoLocalizado = [-60.69001688544, -31.64054974317];
    	var radius = evt.coords.accuracy / 2;
    	
        L.marker([app.puntoLocalizado[1], app.puntoLocalizado[0]], {icon:marcadorGeo}).addTo(app.map);
//        L.circle({lng: app.puntoLocalizado[0], lat: app.puntoLocalizado[1]}, radius).addTo(app.map);
        app.marcarVecinoCercano();
    },

    
	/**
	 * Manejador del evento Geolocalización errónea
	 * @param {error} Object Datos del error
	 * @public
	 */
    onLocationError: function(error){
    	alert(error.message);
    	console.log(error.code + ': ' + error.message);
    },
    
    
	/**
	 * Manejador del botón Geolocalizar
	 * @param {evt} Object Evento
	 * @public
	 */
    onBtnGeolocalizar: function(evt){
    	app.geolocalizar();
    },
    
    
	/**
	 * Manejador del botón Buscar otro amigo
	 * @param {evt} Object Evento
	 * @public
	 */
	onBtnBuscarOtro: function(evt){
		//Sólo si hay punto localizado
		if (app.puntoLocalizado){

			//Si hay punto cercano seleccionado lo borramos 
			if (app.puntoCercano){
				for (i = 0; i < vecinosfinjs.features.length; i++){
					var feature = vecinosfinjs.features[i];
					if (feature.properties.id === app.puntoCercano.properties.id){
						vecinosfinjs.features.splice(i, 1); 	//Borramos
					}
				}

				//Si no quedan vecinos vuelvo a ponerlos todos
				if (vecinosfinjs.features.length === 0){
					vecinosfinjs = jQuery.extend(true, {}, app.vecinosInicial);
				}
			}
			
			//Marcamos el vecino cercano
			app.marcarVecinoCercano();
    	
		} else {
    		alert("No estás geolocalizado!");
    	}
	},
	
	
	
    
	
	/**
	 * Realiza la geolocalización
	 * @public
	 */
    geolocalizar: function(){
		//app.map.locate({setView: true, maxZoom: 17});
    	navigator.geolocation.getCurrentPosition(app.onLocationFound, app.onLocationError);
	},
	
    
	/**
	 * Define el popup a mostrar al seleccionar una feature
	 * @param {feature} Object Feature seleccinada
	 * @return {String} HTML del popup a mostrar
	 * @public
	 */
	popupContent: function(feature){
		return "<h5 class='infoHeader'><strong>"+ feature.properties.nombre +"</strong></h5><hr><p class='infoBody'><strong> Dirección: </strong>" + feature.properties.direcion + "</p><p class='infoBody'><strong> Teléfono: </strong>" + feature.properties.telefono +
			"</p><p class='infoBody'><strong> Horario: </strong>" + feature.properties.horario + "</p><p class='infoBody'><strong> Responsable: </strong>" + feature.properties.responsabl + "</p>";
	},
	
	

	/**
	 * Busca el vecino más cercano al punto geolocalizado y lo marca en el mapa
	 * Si había ya un vecino seleccionado lo elimina 
	 * @public
	 */
	marcarVecinoCercano: function(){
    	//Borramos el anterior marcador de vecino cercano
        if (app.marcadorVecinoCercano){
            app.map.removeLayer(app.marcadorVecinoCercano);
        }

        var options = {units: 'miles'};
        var from = turf.point(app.puntoLocalizado);
        for (i=0; i < vecinosfinjs.features.length; i++){
            var to = turf.point(vecinosfinjs.features[i].geometry.coordinates);
            console.log(vecinosfinjs.features[i].properties.nombre + ": " + turf.distance(from, to, options));        
        }
        console.log("-------------------------------------");        

        
        //Buscamos la feature más cercana
    	app.puntoCercano = turf.nearestPoint(app.puntoLocalizado, vecinosfinjs);

    	//Calculamos las coordenadas: En el vector de geometría del punto cercano, la longitud y latitud están al revés
    	//de como las espera el Marker
        var coordenadasPuntoCercano 	= [app.puntoCercano.geometry.coordinates[1], app.puntoCercano.geometry.coordinates[0]];
        var coordenadasPuntoLocalizado	= [app.puntoLocalizado[1], app.puntoLocalizado[0]]; 	
        
        //Creamos el marcador, lo añadimos al mapa y le asignamos el popup
        app.marcadorVecinoCercano = L.marker(coordenadasPuntoCercano, {icon: app.marcador});
        app.marcadorVecinoCercano.addTo(app.map);
        app.marcadorVecinoCercano.bindPopup(app.popupContent(app.puntoCercano)).openPopup();
        
        //Hacemos el zoom en la vista para que se vea todo
        app.map.fitBounds([coordenadasPuntoLocalizado, coordenadasPuntoCercano]);
    }
	
	
	

 };

//BOTON CONTROL DE LAYERS OCULTAR/MOSTRAR controlCapas
$("#button-layer").click(function(){
	$("#controlCapas").hide();
	$("#controlCapas").toggle();
});
 
 
/////////////////////// 
////// INICIO
/////////////////////// 
if("addEventListener" in document){
	document.addEventListener('DOMContentLoaded', function(){
		app.inicioApp();
		//app.dispositivoListo();
	});
	 
	document.addEventListener('deviceready', function(){
		app.dispositivoListo();
	});
}

 